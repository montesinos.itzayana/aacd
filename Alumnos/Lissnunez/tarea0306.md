Tarea 03 Junio


Con la base flights del paquete nycflights13. Responder las
siguientes preguntas:

1.	¿Cuál fue el peor día para viajar en 2013?
1.1.	Razonamiento & Respuesta: Si el “Peor” es en función del dia que tuvo mayor retraso en la salida fue el 9 de Enero con 1,301 minutos de retraso.

Código:
flights %>%
  filter (year== 2013) %>%
  select(year, month, day,dep_delay)%>%
  arrange(desc(dep_delay))%>%
  top_n(1)

Output:
year month   day dep_delay
   <int> <int> <int>     <dbl>
 1  2013     1     9      1301


1.2.	Razonamiento & Respuesta: Si el “Peor” es en función del dia que tuvo mayor retraso en la llegada fue el 9 de Enero con 1,272 minutos de retraso.
Código:
flights %>%
  filter (year == 2013) %>%
  select(year, month, day,arr_delay)%>%
  arrange(desc(arr_delay))%>%
  top_n(1)
Output:
year month   day arr_delay
   <int> <int> <int>     <dbl>
 1  2013     1     9      1272


2.	¿Cuál es la peor aerolinea con respecto a retrasos?
2.1.	Razonamiento &Respuesta: Si el “Peor” es en función del  mayor retraso en las salidas históricas es F9  
Codigo:
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_salida = mean(dep_delay, na.rm=TRUE))%>%
  arrange(desc(retrasos_salida))%>%
  top_n(1)

Output:
carrier retrasos_salida
  <chr>             <dbl>
1 F9                 20.2

2.2.	Razonamiento &Respuesta: Si el “Peor” es en función del  mayor retraso en las llegadas históricas es  F9
Código: 
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_llegada = mean(arr_delay,na.rm=TRUE))%>%
  arrange(desc(retrasos_llegada))%>%
  top_n(1)

Output:
carrier retrasos_llegada
  <chr>              <dbl>
1 F9                  21.9


3.	¿Cuál es la mejor aerolínea?
3.1.	Razonamiento & Respuesta: Si el “Mejor” es en función del menor retraso en las llegadas históricas es  AS con un promedio de llegada de 9.93 minutos de anticipación 

Codigo:
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_llegada= mean(arr_delay, na.rm=TRUE))%>%
  arrange(retrasos_llegada)%>%
  slice_min(retrasos_llegada)

Output:
carrier retrasos_llegada
  <chr>              <dbl>
1 AS                 -9.93

3.2.	Si el “Mejor” es en función del menor retraso en las salidas históricas es  US con un promedio de llegada de 3.78 minutos de anticipación 

Codigo:
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_salida= mean(dep_delay, na.rm=TRUE))%>%
  arrange(retrasos_salida)%>%
  slice_min(retrasos_salida)

Output:
carrier retrasos_salida
  <chr>             <dbl>
1 US                 3.78


4.	¿Cuál es el destino más frecuente?
Respuesta: ORD
Codigo:
flights %>%
  group_by(dest)%>%
  count( dest ) %>%
  arrange(desc(n))

Output:
dest      n
   <chr> <int>
 1 ORD   17283
 2 ATL   17215
 3 LAX   16174
 4 BOS   15508
 5 MCO   14082
 6 CLT   14064
 7 SFO   13331
 8 FLL   12055
 9 MIA   11728
10 DCA    9705


5.	¿De dónde venían la mayor cantidad de vuelos?
Respuesta:EWR
Codigo:
flights %>%
  group_by(origin)%>%
  count( origin ) %>%
  arrange(desc(n))

Output:
origin      n
  <chr>   <int>
1 EWR    120835
2 JFK    111279
3 LGA    104662


6.	¿Cuáles son las 10 rutas más transitadas?
Pendiente
7.	¿Qué día de la semana es el peor para viajar?

7.1.	Razonamiento & Respuesta: Si el “peor” es en función al dia de la semana con mayor retraso en las llegadas históricas es el jueves con 11 minutos de retrasos en promedio


Codigo: 
flights %>%
  mutate(fecha = as.Date(time_hour), dia_semana= weekdays(fecha)) %>%
  group_by(dia_semana)%>%
  summarise(minutos_promedio_retrasos= mean(arr_delay, na.rm=TRUE)) %>%
  arrange(minutos_promedio_retrasos)%>%
  top_n(1)

Output 
dia_semana minutos_promedio_retrasos
  <chr>                          <dbl>
1 Thursday                        11.0

7.2.	Si el “peor” es en función al dia de la semana con mayor retraso en las salidas históricas es el jueves con 15 minutos de retrasos en promedio
Codigo:
flights %>%
  mutate(fecha = as.Date(time_hour), dia_semana= weekdays(fecha)) %>%
  group_by(dia_semana)%>%
  summarise(minutos_promedio_retrasos= mean(dep_delay, na.rm=TRUE)) %>%
  arrange(minutos_promedio_retrasos)%>%
  top_n(1)

Output:
dia_semana minutos_promedio_retrasos
  <chr>                          <dbl>
1 Thursday                        15.4

