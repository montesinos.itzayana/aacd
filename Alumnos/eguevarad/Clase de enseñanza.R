library (dplyr)
library (nycflights13)
library (ggplot2)

#Vuelos con retraso de >= 2horas
#revisando la data
#forma menos amigable de hacerlo: tabla$columna")

#forma mas amigable de hacerlo: tabla("nombrecolumna")

#lo primero es entneder como esta construia la variable, le damos clic a help

?flights

flight["arr_delay"]

#vamos a filtrar la base de datos

filter(flights, arr_delay >=120)

#el resultado nos muestra que tuvimos 10.2K vuelos con un retraso asi

# 2do ejercicio cuantos vuelos fueorn a houston? (IAH y HOU) y tardaron mas de 120 minu.

#la coma es un agregados de condiciones y sirve como &

flights %>%
  filter (dest== "IAH" | dest== "HOU")

#OTRA OPCION

flights %>%
 filter( dest %in% c('IAH','HOU'))


#otro ejercicio dentro (no lo terminaste de escribir)

flights %>%
  filter(dest %in% c("IAH", "HOU" ))


#OTRO EJERCICIO: LOS QUE LLEGARON +2HORAS TARDE PERO NO SALIDERON TARDE

flights %>%
  filter(arr_delay>=120 & dep_delay<=0)

#NUEVO EJERCICIO: UNA COLUMNA QUE NOS DIGA EL TIEMPO DE PARTIDA Y LA HORA QUE DEBIO 
#PARTIR EN MINUTOS DESDE LAS 12

flights %>%
  mutate(
    dep_time_minutes = dep_time %/%100*60 + dep_time %%100*60, 
    sched_dep_time_minutes = sched_dep_time %/% 100*60 + sched_dep_time %%100
    ) %>%
  select(dep_time, dep_time_minutes)
  
## otra forma de hacerlo

in_minutes <= function(time) {
  return (time %/% 100*60 + time%% 100)
}

flights2 %>%
  mutate(
    dep_time_minutes = in_minutes(dep_time),
    sched_dep_time_minutes = in_minutes(sched_dep_time))
flights2 %>%
  filter(dep_time_minutes>480)
glimpse(flights2)



###OTRO EJERCICIO: decir las aerolinas con mayores retrasos promedio

flights %>%
  group_by(carrier) %>%
  summarise(arr_delay = mean(arr_delay, na.rm=TRUE)) %>%
  arrange(desc(arr_delay)) %>%
  head(n=5)


###OTRO EJERCICIO: que dia es el que hay mas tardanzas

flights %>%
  mutate(
    date = as.Date(time_hour),
    weekday = weekdays (date)
  )%>%
  group_by(weekday) %>%
  summarise(
    arr_delay=mean(arr_delay, na.rm=TRUE)
  ) %>%
  arrange(desc(arr_delay))
  


###OTRO EJERCICIO: que dia es el que hay mas tardanzas

flights %>%
  mutate(
    date = as.Date(time_hour),
    weekday = weekdays (date)
  )%>%
  group_by(weekday) %>%
  summarise(
    arr_delay=mean(arr_delay, na.rm=TRUE),
    n =n()
  ) %>%
  rename(conteo=n, dia_semana=weekday) %>%
  arrange(desc(arr_delay))


# otro, juntar nombres de aerolinea

flights %>%
  left_join(airlines, by=("carrier"="carrier"))%>%
  rename(airline_name=name) %>%
  select(carrier, airline_name)
  





