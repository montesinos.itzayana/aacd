---
title: "Práctica Agile DS"
date: "Julio 16, 2021"
output:
  html_document:
    toc: yes
    toc_depth: 2
    toc_float: true
    theme: spacelab
---

# Visualización



# Objetivo

El proyecto implica una carga importante de trabajo, por lo que les recomiendo que se repartan lo suficientemente bien las tareas a resolver. La base de datos a trabajar está en [esta liga](https://drive.google.com/drive/folders/1t5hvCOw0sHPw27tXKvrUrcHmbfvqQUx2?usp=sharing).

## Equipos

Los equipos deberán tener mínimo de 5 participantes y máximo de 6 participantes y debe haber integrantes de al menos dos países.

## Actividad

- Integren sus equipos!
- Generen su repositorio de Git{hub|lab}.
- Llenen [esta hoja](https://docs.google.com/spreadsheets/d/1dNyQe9FBXhbUC4R-MiZmkXfLxtWSkTx8yB-JxNAscWo/edit?usp=sharing) para tener registro de los equipos.

# Flujo de trabajo

Dado que los equipos estarán distribuídos fuera de sus ubicaciones físicas, tendrán que organizarse de manera distribuída y con tareas establecidas. Cada equipo debe tener su repositorio en Git{hub|lab}, para trabajar y subir su código.

# Entregables

Los entregables son los siguientes:

- Reporte homogéneo por país (uno por país).
- Reporte homogéneo anual (uno por año) 2019.
	- 2019
	- LY2020
	- BGT21VF
	- LE 5+7
- Reporte homogéneo mensual (uno por mes-año)
- **Dashboard** interactivo para explorar la base de datos de manera más amena.


# Evaluación

El día 6 de agosto del 2021, haremos una demostración de los dashboards y reportes que ustedes generaron para todo el equipo.

Es obligatorio asistir a la sesión, ya que será su derecho a calificación del proyecto.


# Acividad

En este momento es tiempo de tener su primer planning!

Dinámica:

- 40 minutos para primer análisis y planeación.
- 20 minutos para sesión de preguntas.
- 30 minutos para tener su primer sprint calculado.
