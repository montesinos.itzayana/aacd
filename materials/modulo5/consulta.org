* Links para facilidad de consulta:
+ [[https://dplyr.tidyverse.org/][Ayuda Dplyr]], [[https://github.com/rstudio/cheatsheets/blob/master/data-transformation.pdf][CheatSheet Dplyr]], [[https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html][Ejemplos varios de dplyr]].
+ [[https://tidyr.tidyverse.org/][Ayuda TidyR]], [[https://github.com/rstudio/cheatsheets/blob/master/data-import.pdf][Cheatsheet TidyR]] (Este es el que se ocupa para pasar de long a wide y viceversa )
+ [[https://bookdown.org/yihui/rmarkdown/][Guía Rmarkdown]] (libro), [[https://www.rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf][cheatsheet Rmarkdown]]
+ [[https://ggplot2.tidyverse.org/][Ayuda ggplot2]] y [[https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf][cheatsheet ggplot2]]
* Libros
** [[https://r4ds.had.co.nz/index.html][R for Data Science]]
** [[https://rstudio-education.github.io/hopr/][Hands on Programing with R]]
** [[https://r-graphics.org/][R Graphics Cookbook]]
** [[https://bookdown.org/yihui/rmarkdown-cookbook/][Rmarkdown Cookbook]]
