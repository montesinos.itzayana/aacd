library(ggplot2)
library(dplyr)
library(tidyr)

datos <- readRDS('datos/data.Rdata')

hcldata <- datos %>%
    group_by(Escenario,Country) %>%
    summarise(NR=sum(NR,na.rm=TRUE),
              vol = sum(VOL..hl., na.rm=TRUE)) %>%
    filter(Country != c('OCI')) #, Country != c('IBL')) %>%

