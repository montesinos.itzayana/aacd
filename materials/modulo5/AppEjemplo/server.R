#
# This is the server logic of a Shiny web application. You can run the
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

# Define server logic required to draw a histogram
shinyServer(function(input, output) {


    output$distPlot <- renderPlot({
        hcldata %>%
            filter(Country==input$pais) %>%
            group_by(Escenario) %>%
            summarise(NRHTL = sum(NR)/sum(vol)) %>%
            ggplot(aes(x=Escenario, y = NRHTL))+
            geom_bar(stat='identity')

    })

    output$table <- renderDataTable(
        hcldata %>%
        filter(Country==input$pais) %>%
        group_by(Escenario) %>%
        summarise(NRHTL = sum(NR)/sum(vol)) %>%
        pivot_wider(names_from=Escenario, values_from=NRHTL ),
        options = list(
            pageLength = 10
        )
    )

    output$EscenarioSelect <- renderUI({
        ## options <- sort(unique(hcldata$Escenario))
        options <- hcldata %>%
            ungroup() %>%
            select(Escenario) %>%
            distinct()
        selectInput(
            inputId = "Escenario",
            label = "Race",
            choices = c("All", options),
            selected = "All"
        )

  })
})
